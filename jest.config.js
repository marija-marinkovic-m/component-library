module.exports = {
  bail: true,
  verbose: true,
  setupFiles: ['<rootDir>/jest.setup.js'],
  testRegex: '.*-test\\.js$',
  testPathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/.storybook/',
    '<rootDir>/.out'
  ]
};