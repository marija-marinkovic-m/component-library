import React from 'react';
import styled from 'styled-components';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import { Button, Icon, Layer, Avatar } from '.';
import Row from './util/Row';

import faBell from '@fortawesome/fontawesome-pro-light/faBell';
import faEnvelope from '@fortawesome/fontawesome-pro-light/faEnvelope';
import faSpinnerThird from '@fortawesome/fontawesome-pro-light/faSpinnerThird';
import childAvatar from '../static/img/child-avatar.png';
import girlAvatar from '../static/img/girl-avatar.png';

const MarginAvatar = styled(Avatar)`
  margin: 10px;
`;
const OrangeAvatar = styled(MarginAvatar)`
  background-color: orangered;
  color: whitesmoke;
`;
const GreenAvatar = styled(MarginAvatar)`
  background-color: olivedrab;
  color: white;
`;
const LayerStyled = styled(Layer)`
  margin-right: 20px;
`;


const buttonProps = [
  { size: 'small', 'kind': 'primary' },
  { size: 'medium', 'kind': 'secondary' },
  { size: 'large', 'kind': 'success' },
  { size: 'medium', 'kind': 'ghost', disabled: true },
];

storiesOf('Getting Started', module)
  .add('Overview', withInfo({
    text: `
      m Front consists of lots of custom elements repeating across screens, so packaging and distributing them in form of a library can make our code simpler and cleaner, and the development workflow more efficient, hence __m ui library__.
    
      These are simple basic components like texts, icons, buttons or form inputs. Or complex component blocks like AppBar, sophisticated inputs(i.e.typeahead), menus, popovers, etc. Any large project reaches a point when it 's hard to remember and control all the pages and their components.

      Component library should be a special place to store and manage all the HTML components of a web site in one place.`,
    propTablesExclude: [Row, GreenAvatar, MarginAvatar, OrangeAvatar, LayerStyled, Icon, Layer],
    propTables: [Avatar]
  })(() => {
    return (
      <div>
        <h2>Avatar</h2>
        <Row justifyContent="flex-start">
          <MarginAvatar src={childAvatar} imgProps={{width: '96px'}} />
          <MarginAvatar src={girlAvatar} imgProps={{width: '76px'}} />
          <OrangeAvatar imgProps={{width: '52px'}}>
            <Icon glyph={faBell} />
          </OrangeAvatar>
          <GreenAvatar>
            XO
          </GreenAvatar>
          <OrangeAvatar imgProps={{width: '36px'}}>
            S
          </OrangeAvatar>
        </Row>

        <h2>Button</h2>
        <Row justifyContent="flex-start">
          { buttonProps.map(b => <Button {...b} style={{marginRight: '10px'}}>Button</Button>) }
        </Row>

        <h2>Icon</h2>
        <Row justifyContent="flex-start">
          <LayerStyled size="3x">
            <Icon glyph={faBell} />
            <Layer layersType="counter">15</Layer>
          </LayerStyled>
          <LayerStyled size="5x">
            <Icon glyph={faEnvelope} />
            <Layer layersType="counter">99+</Layer>
          </LayerStyled>
          <LayerStyled size="2x">
            <Icon glyph={faSpinnerThird} spin />
          </LayerStyled>
        </Row>
      </div>
    );
  }));