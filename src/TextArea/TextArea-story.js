import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import { boolean, text } from '@storybook/addon-knobs';

import { TextArea } from '.';

storiesOf('Components | TextArea', module)
  .add('Basic', withInfo({
    text: `TextArea Component Description`
  })(() => {
    return <TextArea value={text('Value', 'some value')} />;
  }));