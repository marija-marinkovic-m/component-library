import * as React from 'react';
import { TextAreaProps } from '.';

declare const TextArea: React.ComponentType<TextAreaProps>;

export default TextArea;