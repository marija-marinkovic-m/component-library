import React from 'react';
import styled from 'styled-components';

import baseStyle from '../Form/FieldBase';
import { styles, textAreaPropTypes } from './assets';

class UncontrolledTextAreaComponent extends React.Component {
  _inputRef;
  focus = () => {
    this._inputRef.focus();
  };

  render() {
    return (
      <textarea
        ref={el => {this._inputRef = el}}
        {...this.props} />
    );
  }
}

const UncontrolledTextArea = styled(UncontrolledTextAreaComponent)`
  ${baseStyle};
  height: auto;
  resize: ${styles.resize};
`;

UncontrolledTextArea.displayName = 'UncontrolledTextArea';
UncontrolledTextArea.propTypes = textAreaPropTypes;

export default UncontrolledTextArea;