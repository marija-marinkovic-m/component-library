import * as React from 'react';
import { TextAreaProps } from '.';

declare const UncontrolledTextArea: React.ComponentType<TextAreaProps>;

export default UncontrolledTextArea;