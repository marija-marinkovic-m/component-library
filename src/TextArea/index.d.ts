import * as React from 'react';
import { StandardProps } from '..';

export interface TextAreaProps extends StandardProps<React.HTMLAttributes<HTMLTextAreaElement>> {
  rows?: number;
  resize?: boolean;
  disabled?: boolean;
  invalid?: boolean;
}

export { default as TextArea } from './TextArea';
export { default as UncontrolledTextArea } from './UncontrolledTextArea';