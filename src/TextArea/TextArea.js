import React from 'react';
import UncontrolledTextArea from './UncontrolledTextArea';
import { textAreaPropTypes } from './assets';

class TextArea extends React.Component {
  static displayName = 'TextArea';
  static propTypes = textAreaPropTypes;
  state = {
    value: this.props.value || ''
  }
  handleChange = (event) => {
    this.setState({value: event.target.value});
    if (this.props.onChange) this.props.onChange(event);
  }
  _inputRef;
  focus = () => {
    this._inputRef.focus();
  }

  render() {
    return (
      <UncontrolledTextArea
        value={this.state.value}
        onChange={this.handleChange}
        {...this.props}
        ref={el => {this._inputRef = el}} />
    );
  }
}

export default TextArea;