import PropTypes from 'prop-types';

export const textAreaPropTypes = {
  rows: PropTypes.number,
  resize: PropTypes.bool,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  invalid: PropTypes.bool
};

export const styles = {
  resize({resize}) {
    return resize ? 'auto' : 'none';
  }
}