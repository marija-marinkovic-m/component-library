import React from 'react';
import PropTypes from 'prop-types';

class List extends React.Component {
  getChildContext() {
    return {
      dense: this.props.dense
    };
  }

  render() {
    const {
      children, className, component: Component,
      dense, disablePadding, subheader, ...other
    } = this.props;

    return (
      <Component className={className}>
        {subheader}
        {children}
      </Component>
    );
  }
}

List.propTypes = {
  children: PropTypes.node,
  /**
   * @ignore
   */
  className: PropTypes.string,
  component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  dense: PropTypes.bool,
  disablePadding: PropTypes.bool,
  subheader: PropTypes.node
};

List.defaultProps = {
  component: 'ul',
  dense: false,
  disablePadding: false
};

List.childContextTypes = {
  dense: PropTypes.bool
};

export default List;