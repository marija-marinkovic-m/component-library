import React from 'react';
import PropTypes from 'prop-types';

class ListItem extends React.Component {
  getChildContext() {
    return {
      dense: this.props.dense || this.context.dense || false
    };
  }

  render() {
    const { children, component: Component, dense, disabled, disableGutters, divider, ...other } = this.props;


    return (
      <Component>
        {children}
      </Component>
    );
  }
}

ListItem.propTypes = {
  /**
   * if not null the list item will be a clicable button
   */
  onClick: PropTypes.func,
  /**
   * @ignore
   */
  className: PropTypes.string,
  component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  dense: PropTypes.bool,
  disabled: PropTypes.bool,
  disableGutters: PropTypes.bool,
  divider: PropTypes.bool
};

ListItem.defaultProps = {
  onClick: null,
  dense: false,
  disabled: false,
  disableGutters: false,
  divider: false,
  component: 'li'
};

ListItem.contextTypes = {
  dense: PropTypes.bool,
};

ListItem.childContextTypes = {
  dense: PropTypes.bool,
};

export default ListItem;