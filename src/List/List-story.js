import React from 'react';
import { storiesOf } from '@storybook/react';

import List from './List';
import ListItem from './ListItem';

const items = [1, 2, 3, 4]

storiesOf('Components | List', module)
  .add('List Item', () => {
    return (
      <List>
        {items.map(e => <ListItem>{e}</ListItem>)}
      </List>
    );
  });