import * as React from 'react';
import { StandardProps } from '..';

export interface IconProps extends StandardProps<React.HTMLAttributes<HTMLDivElement>> {
  glyph: string | object | Array<string>;
  mask?: string | object | Array<string>;
  border?: boolean;
  fixedWidth?: boolean;
  flip?: 'horizontal' | 'vertical' | 'both';
  listItem?: boolean;
  pull?: 'right' | 'left';
  pulse?: boolean;
  rotation?: 90 | 180 | 270;
  size?: 'lg' | 'xs' | 'sm' | '1x' | '2x' | '3x' | '4x' | '5x' | '6x' | '7x' | '8x' | '9x' | '10x';
  spin?: boolean;
  transform?: string | object;
  inverse?: boolean;
}

declare const Icon: React.ComponentType<IconProps>;

export default Icon;