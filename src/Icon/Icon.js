import React from 'react';
import PropTypes from 'prop-types';

import fontawesome from '@fortawesome/fontawesome';
import convert from './converter';

function classList (props) {
  let classes = {
    'fa-spin': props.spin,
    'fa-pulse': props.pulse,
    'fa-fw': props.fixedWidth,
    'fa-border': props.border,
    'fa-li': props.listItem,
    'fa-inverse': props.inverse,
    'fa-flip-horizontal': props.flip === 'horizontal' || props.flip === 'both',
    'fa-flip-vertical': props.flip === 'vertical' || props.flip === 'both',
    [`fa-${props.size}`]: props.size !== null,
    [`fa-rotate-${props.rotation}`]: props.rotation !== null,
    [`fa-pull-${props.pull}`]: props.pull !== null,
    [`fa-layers-${props.layersType}`]: props.layersType !== null
  };
  return Object.keys(classes)
    .map(key => classes[key] ? key : null)
    .filter(key => key);
}

function normalizeIconArgs (icon) {
  if (icon === null) {
    return null;
  }

  if (typeof icon === 'object' && icon.prefix && icon.iconName) {
    return icon;
  }

  if (Array.isArray(icon) && icon.length === 2) {
    return { prefix: icon[0], iconName: icon[1] };
  }

  if (typeof icon === 'string') {
    return { prefix: 'fas', iconName: icon };
  }
}

function objectWithKey (key, value) {
  return ((Array.isArray(value) && value.length > 0) || (!Array.isArray(value) && value)) ? {[key]: value} : {};
}

function Icon (props) {
  const { mask: maskArgs, transform: transformArg, glyph, className } = props;
  const icon = normalizeIconArgs(glyph);
  const classes = objectWithKey('classes', [...classList(props), ...className.split(' ')]);
  const mask = objectWithKey('mask', normalizeIconArgs(maskArgs));
  const transform = objectWithKey('transform', (typeof transformArg === 'string') ? fontawesome.parse.transform(transformArg) : transformArg );

  const renderedIcon = fontawesome.icon(icon, {
    ...classes,
    ...transform,
    ...mask
  });

  if (!renderedIcon) {
    console.log('Could not find icon', icon);
    return null;
  }

  const { abstract } = renderedIcon
  const convertCurry = convert.bind(null, React.createElement)
  const extraProps = {}

  Object.keys(props).forEach(key => {
    if (!Icon.defaultProps.hasOwnProperty(key)) extraProps[key] = props[key]
  })

  return convertCurry(abstract[0], extraProps);

}

Icon.propTypes = {
  border: PropTypes.bool,
  className: PropTypes.string,
  mask: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
  fixedWidth: PropTypes.bool,
  flip: PropTypes.oneOf(['horizontal', 'vertical', 'both']),
  listItem: PropTypes.bool,
  pull: PropTypes.oneOf(['right', 'left']),
  pulse: PropTypes.bool,
  rotation: PropTypes.oneOf([90,180,270]),
  size: PropTypes.oneOf(['lg', 'xs', 'sm', '1x', '2x', '3x', '4x', '5x', '6x', '7x', '8x', '9x', '10x']),
  spin: PropTypes.bool,
  glyph: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
  transform: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  inverse: PropTypes.bool
}

Icon.defaultProps = {
  border: false,
  className: '',
  mask: null,
  fixedWidth: false,
  flip: null,
  glyph: null,
  listItem: false,
  pull: null,
  pulse: false,
  rotation: null,
  size: null,
  spin: false,
  transform: null,
  inverse: false
}


export class Layer extends React.PureComponent {
  render () {
    const {children, className, style: styleArg, transform: transformArg, ...props} = this.props;

    const wrapperProps = {};

    const baseClasses = className + (props.layersType === null && ' fa-layers');
    const propClasses = classList(props).join(' ');

    wrapperProps.className = `${propClasses} ${baseClasses}`;

    if (transformArg) {
      wrapperProps['data-fa-transform'] = transformArg;
    }
    if (styleArg) {
      wrapperProps['style'] = styleArg;
    }

    return (<div {...wrapperProps}>{ children }</div>);
  }
}

Layer.propTypes = {
  layersType: PropTypes.oneOf(['text', 'counter']),
  style: PropTypes.object,
  children: PropTypes.node,
  className: PropTypes.string,
  fixedWidth: PropTypes.bool,
  size: PropTypes.oneOf(['lg', 'xs', 'sm', '1x', '2x', '3x', '4x', '5x', '6x', '7x', '8x', '9x', '10x']),
  rotation: PropTypes.oneOf([90,180,270]),
  pull: PropTypes.oneOf(['right', 'left']),
  transform: PropTypes.string
}
Layer.defaultProps = {
  layersType: null,
  className: '',
  fixedWidth: false,
  size: null,
  rotation: null,
  pull: null,
  transform: null
}

export default Icon;