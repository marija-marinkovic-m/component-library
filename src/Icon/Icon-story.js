import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import TableComponent from '../util/storybook-TableComponent';

import Icon, { Layer } from './Icon';
import Button from '../Button';

import fontawesome from '@fortawesome/fontawesome';
import light from '@fortawesome/fontawesome-pro-light';
import farCalendarAlt from '@fortawesome/fontawesome-pro-regular/faCalendarAlt';

import solid from '@fortawesome/fontawesome-pro-solid';
import fabFacebook from '@fortawesome/fontawesome-free-brands/faFacebookF';

import { selectV2 as select, boolean, number } from '@storybook/addon-knobs';
import styled from 'styled-components';

fontawesome.library.add(
  light,
  solid,
  farCalendarAlt,
  fabFacebook
);

const iconGlyphs = {
  Home: ['fal', 'home'],
  'Table Grid': ['fal', 'th'],
  Drive: ['fal', 'hdd'],
  Envelope: ['fal', 'envelope-open'],
  Comment: ['fal', 'comment'],
  Calendar: ['far', 'calendar-alt'],
  User: ['fal', 'user'],
  Globe: ['fal', 'globe'],
  Bell: ['fal', 'bell']
};
const iconFlip = {
  None: null,
  Horizontal: 'horizontal',
  Vertical: 'vertical',
  Both: 'both'
};
const iconRotation = {
  None: null,
  '90': 90,
  '180': 180,
  '270': 270
};
const iconPull = {
  None: null,
  Left: 'left',
  Right: 'right'
};
const iconSizes = {
  Large: 'lg',
  Small: 'sm',
  ExtraSmall: 'xs',
  '1x': '1x',
  '2x': '2x',
  '3x': '3x',
  '4x': '4x',
  '5x': '5x',
  '6x': '6x',
  '7x': '7x',
  '8x': '8x',
  '9x': '9x',
  '10x': '10x',
}

const IconStyled = styled(Icon)`
  margin-right: 10px;
  background-color: mistyrose;
`;
const SpanStyled = styled.span`
  margin-right: 10px;
  background-color: mistyrose;
`;
const LayerStyled = styled(Layer)`
  margin-right: 10px;
  background-color: mistyrose;
`;
const IconEl = ({glyph = ['fal', 'magic'], size = '4x', ...props}) => <IconStyled
{...props}
glyph={glyph}
size={size} />;

storiesOf('Components | Icon', module)
  .add('Basic Features', withInfo(`Basic features like spin and pulse animations, fixed width borders, flip direction, slize, pull or rotate.`)(() => {
    // general
    const glyph = select('Glyph', iconGlyphs, 'fal,home', 'General');
    const size = select('Size', iconSizes, '10x', 'General');
    const border = boolean('Border', true, 'General');
    
    // animations
    const spin = boolean('Spin', false, 'Animation');
    const pulse = boolean('Pulse', false, 'Animation');

    // positioning
    const fixedWidth = boolean('Fixed Width', true, 'Positioning');
    const flip = select('Flip', iconFlip, null, 'Positioning');
    const rotation = select('Rotation', iconRotation, null, 'Positioning');
    const pull = select('Pull', iconPull, null, 'Positioning');

    return <Icon
      glyph={glyph.split(',')}
      size={size}
      spin={spin}
      pulse={pulse}
      fixedWidth={fixedWidth}
      border={border}
      flip={flip}
      rotation={rotation}
      pull={pull} />
  }))
  .add('Transforms', withInfo({
    propTables: [Icon],
    propTablesExclude: [IconEl],
    text: `
    Fontawesome 5 "Power Transforms".  
    Thanks to the SVG approach, icons now can be scaled, positioned, flipped and rotated arbitratily using **\`transform\`** property.  
    &nbsp;  

    ##### Scaling
       Change icon size without changing or moving the container.  
       To scale icons up or down, use **\`grow-#\`** and **\`shrink-#\`** with any arbitrary value, including decimals. Units are 1/16em.  
       For clarity in the example, there is background color on the icon so the effect can be clear.  
       &nbsp;  

    #### Positioning
       Change icon location without changing or moving the container. To move icons up, down, left, or right, use **\`up-#\`**, **\`down-#\`**, **\`left-#\`**, and **\`right-#\`** with any arbitrary value, including decimals. Units are 1/16em.  
       &nbsp;  

    #### Rotating & Flipping
       Change icon angle and reflection without changing or moving the container.  
       To rotate or flip icons use any combination of **\`rotate-#\`**, **\`flip-v\`**, and **\`flip-h\`** with any arbitrary value.  
       Units are degrees with negative numbers allowed.  
       &nbsp;  

    `
  })(() => {
    // scaling
    const shrink = number('Shrink', 8, {}, 'Scaling');
    const grow = number('Grow', 6, {}, 'Scaling');

    // positioning
    const up = number('Up', 6, {}, 'Positioning');
    const right = number('Right', 6, {}, 'Positioning');
    const down = number('Down', 6, {}, 'Positioning');
    const left = number('Left', 6, {}, 'Positioning');

    // rotating & flipping
    const rotate = number('Rotate', -10, {}, 'Rotating & Flipping');

    return (
      <div>
        <h3>Scaling</h3>
        <div>
          <IconEl />
          <IconEl transform={`shrink-${shrink}`} />
          <IconEl transform={`grow-${grow}`} />
        </div>

        <h3>Positioning</h3>
        <div>
          <IconEl transform={`shrink-8 up-${up}`} />
          <IconEl transform={`shrink-8 right-${right}`} />
          <IconEl transform={`shrink-8 down-${down}`} />
          <IconEl transform={`shrink-8 left-${left}`} />
        </div>

        <h3>Rotating & Flipping</h3>
        <div>
          <IconEl transform={`rotate-${rotate}`} />
          <IconEl transform={`flip-v`} />
          <IconEl transform={`flip-h`} />
        </div>
      </div>
    );
  }))
  .add('Composition', withInfo({
    propTables: [Icon],
    propTablesExclude: [IconEl],
    text: `
      #### Masking  
         Combine two icons to create one single-color shape.  
         Masks are great when you _do_ want your background color to show through. For clarity in the example, background color added to the icon.  
         &nbsp;  
    `
  })(() => {
    return (<div>
      <IconEl
        glyph={['fas', 'pencil-alt']}
        mask={['fas', 'comment']}
        transform="shrink-10 up-.5" />
      <IconEl
        glyph={['fal', 'alarm-clock']}
        mask={['fas', 'square']}
        transform="shrink-6" />
      <IconEl
        glyph={['fab', 'facebook-f']}
        mask={['fas', 'circle']}
        transform="shrink-3.5 down-1.6 right-1.25" />
    </div>);
  }))
  .add('Layering', withInfo({
    text: `
      #### Layering, Text, & Counters  
      &nbsp;  
      Layers are the new way to place icons and text visually on top of each other, replacing our classic icons stacks. With this new approach you can use more than 2 icons.  
      &nbsp;  

      Layers are good when you don’t want page’s background to show through, or when you do want to use multiple colors, layer several icons, layer text, or layer counters onto an icon.  
      For clarity in the example, a background color added on the layers so you can see the effect.
    `,
    propTablesExclude: [LayerStyled],
    TableComponent
  })(() => {
    return (
      <div className="fa-5x">
        <LayerStyled>
          <Icon glyph={['fal', 'square']} color="green" />
          <Icon glyph={['fal', 'check']} color="green" transform="shrink-6" />
        </LayerStyled>
        <LayerStyled fixedWidth>
          <Icon glyph={['fas', 'bookmark']} />
          <Icon glyph={['fas', 'star']} color="#be0000" inverse transform="shrink-10 up-2" />
        </LayerStyled>

        <LayerStyled fixedWidth>
          <Icon glyph="play" transform="rotate--90 grow-2" />
          <Icon glyph="sun" inverse transform="shrink-10 up-2" />
          <Icon glyph="moon" inverse transform="shrink-11 down-4.2 left-4" />
          <Icon glyph="star" inverse transform="shrink-11 down-4.2 right-4" />
        </LayerStyled> 

        <LayerStyled>
          <Icon glyph="calendar" />
          <Layer layersType="text" inverse fixedWidth transform="shrink-8 down-3" style={{fontWeight: 900}}>31</Layer>
        </LayerStyled>

        <LayerStyled>
          <Icon glyph="certificate" />
          <Layer
            layersType="text"
            inverse
            transform="shrink-11.5 rotate--30"
            style={{fontWeight: 900}}>NEW</Layer>
        </LayerStyled>

        <LayerStyled>
          <Icon glyph="envelope" />
          <Layer
            layersType="counter"
            style={{background: 'Tomato'}}>1,419</Layer>
        </LayerStyled>
      </div>
    );
  }));