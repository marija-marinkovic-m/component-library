import React from 'react';
import PropTypes from 'prop-types';
import calculatePosition from '../util/calculate-position';
import RefComponent from '../util/Ref';
import styled from 'styled-components';


const Helper = styled.div`
  position: absolute; top: 0px; left: 0px;
`;
const Overlay = styled.div`
  position: fixed; top: 0; bottom: 0; left: 0; right: 0;
  ${({modal}) => modal && 'z-index: 999; display: flex;'}
  background: ${({modal}) => modal ? 'rgba(0,0,0,.5)' : 'transparent'};
`;
const Content = styled.div`
  position: ${({modal}) => modal ? 'relative': 'absolute'};
  z-index: 2;
  padding: 5px;
  ${({modal}) => modal && 'margin: auto;'}
  background: rgb(255,255,255);
  border: 1px solid rgb(187,187,187);
  ${({modal}) => !modal && 'box-shadow: rgba(0,0,0,.2) 0px 1px 3px;'}
`;
const Arrow = styled.div`
  position: absolute; z-index: -1;
  width: 10px; height: 10px; margin: -5px;
  background: rgb(255,255,255);
  box-shadow: rgba(0,0,0,.2) 1px 1px 1px;
  transform: rotate(45deg);
`;

const PositionEnums = [
  'top left', 'top center', 'top right',
  'bottom left', 'bottom center', 'bottom right',
  'right top', 'right center', 'right bottom',
  'left top', 'left center', 'left bottom'
];

/** Popover component description */
class Popover extends React.PureComponent {
  state = {
    isOpen: this.props.open || this.props.defaultOpen,
    modal: this.props.modal ? true : !this.props.trigger // popover can't be a tooltip if the trigger prop doesn't exist
  }

  _helperEl = null;
  _arrowEl = null;
  _triggerEl = null;
  _contentEl = null;

  constructor(props) {
    super(props);
    this.timeOut = 0;
  }

  componentDidMount() {
    const { closeOnEscape, defaultOpen } = this.props;
    if (defaultOpen) this.setPosition();
    if (closeOnEscape) {
      window.addEventListener('keyup', e => {
        if (e.key === 'Escape') this.closePopover();
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.open === nextProps.open) return;
    if (nextProps.open) {
      return this.openPopover();
    }
    this.closePopover();
  }

  componentWillUnmount() {
    clearTimeout(this.timeOut);
  }

  lockScroll = () => {
    if (this.state.modal && this.props.lockScroll) {
      document.getElementsByTagName('body')[0].style.overflow = 'hidden';
    }
  }

  resetScroll = () => {
    if (this.state.modal && this.props.lockScroll) {
      document.getElementsByTagName('body')[0].style.overflow = 'auto';
    }
  }

  togglePopover = () => {
    if (this.state.isOpen) {
      return this.closePopover();
    }
    this.openPopover();
  }

  openPopover = () => {
    if (this.state.isOpen) return;
    this.setState({isOpen: true}, () => {
      this.setPosition();
      this.props.onOpen();
      this.lockScroll();
    });
  }

  closePopover = () => {
    if (!this.state.isOpen) return;
    this.setState({isOpen: false}, () => {
      this.props.onClose();
      this.resetScroll();
    });
  }

  onMouseEnter = () => {
    clearTimeout(this.timeOut);
    this.timeOut = setTimeout(() => this.openPopover(), this.props.mouseEnterDelay);
  }

  onMouseLeave = () => {
    clearTimeout(this.timeOut);
    this.timeOut = setTimeout(() => this.closePopover(), this.props.mouseLeaveDelay);
  }

  setPosition = () => {
    if (this.state.modal || !this._helperEl || !this._contentEl || !this._triggerEl) return;
    const { arrow, position, offsetX, offsetY } = this.props;
    
    const helper = this._helperEl.getBoundingClientRect();
    const trigger = this._triggerEl.getBoundingClientRect();
    const content = this._contentEl.getBoundingClientRect();
    const coords = calculatePosition(trigger, content, position, arrow, {offsetX, offsetY});

    this._contentEl.style.top = coords.top - helper.top + 'px';
    this._contentEl.style.left = coords.left - helper.left + 'px';

    if (arrow && this._arrowEl) {
      this._arrowEl.style['transform'] = coords.transform;
      this._arrowEl.style['-ms-transform'] = coords.transform;
      this._arrowEl.style['-webkit-transform'] = coords.transform;
      this._arrowEl.style.top = coords.arrowTop;
      this._arrowEl.style.left = coords.arrowLeft;
    }

    if (window.getComputedStyle(this._triggerEl, null).getPropertyValue('position') == 'static' || window.getComputedStyle(this._triggerEl, null).getPropertyValue('position') == '') {
      this._triggerEl.style.position = 'relative';
    }
  }

  getContentProps = () => {
    const { className, on } = this.props;
    const { modal } = this.state;

    const contentProps = {
      modal,
      key: 'C',
      className: `popover-content ${className}`,
      innerRef: el => this._contentEl = el,
      onClick: e => e.stopPropagation()
    };

    if (!modal && on.includes('hover')) {
      contentProps.onMouseEnter = this.onMouseEnter;
      contentProps.onMouseLeave = this.onMouseLeave;
    }

    return contentProps;
  }

  renderTrigger = () => {
    const triggerProps = { key: 'T' };
    const { on, trigger } = this.props;
    const onArray = Array.isArray(on) ? on : [on];

    for (let i = 0; i < onArray.length; i++) {
      switch (onArray[i]) {
        case 'click':
          triggerProps.onClick = this.togglePopover;
          break;
        case 'hover':
          triggerProps.onMouseEnter = this.onMouseEnter;
          triggerProps.onMouseLeave = this.onMouseLeave;
          break;
        case 'focus':
          triggerProps.onFocus = this.onMouseEnter;
          break;
      }
    }

    if (typeof trigger === 'function') return React.cloneElement(trigger(this.state.isOpen), triggerProps);

    return React.cloneElement(trigger, triggerProps);
  }

  renderContent = () => {
    const { arrow } = this.props;
    const { modal } = this.state;
    return (
      <Content {...this.getContentProps()}>
        {
          arrow && !modal && (<Arrow innerRef={el => this._arrowEl = el} />)}
        {
          typeof this.props.children === 'function'
          ? this.props.children(this.closePopover, this.state.isOpen)
          : this.props.children
        }
      </Content>
    );
  }

  render() {
    const { closeOnDocumentClick } = this.props;
    const { modal, isOpen } = this.state;

    return [
      isOpen && <Helper key="H" innerRef={el => this._helperEl = el} />,
      isOpen && (
        <Overlay key="O" className="popover-overlay" modal={modal} onClick={closeOnDocumentClick ? this.closePopover : () => {}}>
          { modal && this.renderContent() }
        </Overlay>
      ),
      isOpen && !modal && this.renderContent(),
      !!this.props.trigger && (
        <RefComponent innerRef={el => this._triggerEl = el} key="R">
          { this.renderTrigger() }
        </RefComponent>
      )
    ];
  }
}

Popover.displayName = 'Popover';

Popover.propTypes = {
  className: PropTypes.string,
  modal: PropTypes.bool,
  closeOnDocumentClick: PropTypes.bool,
  closeOnEscape: PropTypes.bool,
  lockScroll: PropTypes.bool,
  offsetX: PropTypes.number,
  offsetY: PropTypes.number,
  mouseEnterDelay: PropTypes.number,
  mouseLeaveDelay: PropTypes.number,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool,
  defaultOpen: PropTypes.bool,
  /**
   * Trigger element
   * - for uncontrolled component we don't need the trigger Element
   */
  trigger: PropTypes.oneOfType([PropTypes.func, PropTypes.element]),
  on: PropTypes.oneOfType([
    PropTypes.oneOf(['hover', 'click', 'focus']),
    PropTypes.arrayOf(PropTypes.oneOf(['hover', 'click', 'focus']))
  ]),
  children: PropTypes.oneOfType([
    PropTypes.func, PropTypes.element, PropTypes.string
  ]).isRequired,
  /** 
   * @ignore 
   */
  position: PropTypes.oneOf(PositionEnums)
};

Popover.defaultProps = {
  children: () => <span>Your content goes here!!!</span>,
  trigger: null,
  onOpen: () => {},
  onClose: () => {},
  defaultOpen: false,
  open: false,
  closeOnDocumentClick: false,
  closeOnEscape: true,
  on: ['click'],
  className: '',
  position: 'bottom center',
  modal: false,
  lockScroll: true,
  arrow: true,
  offsetX: 0,
  offsetY: 0,
  mouseEnterDelay: 100,
  mouseLeaveDelay: 100
};

export default Popover;