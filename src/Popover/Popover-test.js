import React from 'react';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import Popover from './Popover';

describe('Popover', () => {
  describe('rendering', () => {

    it('should render correctly', () => {
      const popover = renderer.create(<Popover trigger={<button>Trigger</button>}>
        Popover Content
      </Popover>);
      const rendered = popover.toJSON();
      expect(rendered).toMatchSnapshot();
    });

    it('should render correctly on click (default)', () => {
      const clickPopover = mount(<Popover trigger={<button>T</button>}>
        <p className="content-p">Popover content</p>
      </Popover>);
      clickPopover.find('button').simulate('click');
      expect(clickPopover.find('.content-p').length).toEqual(1);
    });

    it('should render correctly on mouseEnter (with delay specified)', () => {
      const mouseEnterDelay = 10;
      const hoverPopover = mount(<Popover
          on="hover"
          trigger={<button id="t">Trigger</button>}
          mouseEnterDelay={mouseEnterDelay}>
        <p className="content-p">Popover content</p>
      </Popover>);

      // before mouseover
      expect(hoverPopover.find('.content-p').length).toEqual(0);

      // simulate mouseEnter
      hoverPopover.find('#t').simulate('mouseEnter');

      // after
      expect(hoverPopover.find('.content-p').length).toEqual(0);
      setTimeout(() => expect(hoverPopover.find('.content-p').length).toEqual(1), mouseEnterDelay);
    });

    it('should render trigger element provided as a function and apply `on` correctly', () => {
      const triggerFn = (isOpen) => (<button>Trigger element, current popover state: <span className="popover-curr-state">{isOpen ? 'opened' : 'closed'}</span></button>);
      const popover = mount(<Popover on="click" trigger={triggerFn}>
        <p className="content-p">Popover content</p>
      </Popover>);

      expect(popover.find('.popover-curr-state').text()).toEqual('closed');
      popover.find('button').simulate('click');
      expect(popover.find('.popover-curr-state').text()).toEqual('opened');
    });

    it('should render content element provided as a function and read states correctly', () => {
      const contentFn = (closeFn, isOpen) => (<p>Content element, current popover state: <span className="popover-curr-state">{ isOpen ? 'opened' : 'closed' }</span><button className="close-trigger" onClick={closeFn}>Close</button></p>);
      const popover = mount(<Popover on="click" trigger={<button className="open-trigger">T</button>}>
        { contentFn }
      </Popover>);

      expect(popover.find('.popover-curr-state').length).toEqual(0);
      popover.find('.open-trigger').simulate('click');
      expect(popover.find('.popover-curr-state').text()).toEqual('opened');

      // close the popover from popover content
      popover.find('.close-trigger').simulate('click');
      expect(popover.find('.popover-curr-state').length).toEqual(0);
    });

  });

  describe('interaction', () => {

    it('should not close on document click (default)', () => {
      const popover = mount(<Popover trigger={<button>Trigger</button>}><p className="content-p">&nbsp;</p></Popover>);
      popover.find('button').simulate('click');

      expect(popover.find('.content-p').length).toEqual(1);
      popover.find('div.popover-overlay').simulate('click');
      expect(popover.find('.content-p').length).toEqual(1);
    });
    it('should close on document click (closeOnDocumentClick)', () => {
      const popover = mount(<Popover closeOnDocumentClick trigger={<button>Trigger</button>}><p className="content-p">&nbsp;</p></Popover>);
      popover.find('button').simulate('click');

      expect(popover.find('.content-p').length).toEqual(1);
      popover.find('div.popover-overlay').simulate('click');
      expect(popover.find('.content-p').length).toEqual(0);
    });
  });

  describe('position', () => {
    const PopoverTest = (props) => <Popover {...props} trigger={<button>Trigger</button>}><p>content</p></Popover>;

    it('should render in the bottom center (default)', () => {
      const popover = renderer.create(<PopoverTest />);
      const rendered = popover.toJSON();
      expect(rendered).toMatchSnapshot();
    });

    it('should render in the `top left`', () => {
      const popover = renderer.create(<PopoverTest position="top left" />);
      const rendered = popover.toJSON();
      expect(rendered).toMatchSnapshot();
    });

    it('should render in the `right center`', () => {
      const popover = renderer.create(<PopoverTest position="right center" />);
      const rendered = popover.toJSON();
      expect(rendered).toMatchSnapshot();
    });
  });
});