import * as React from 'react';
import { StandardProps } from '..';

export interface PopoverProps extends StandardProps<React.HTMLAttributes<HTMLDivElement>> {
  modal?: boolean;
  closeOnDocumentClick?: boolean;
  closeOnEscape?: boolean;
  lockScroll?: boolean;
  offsetX?: number;
  offsetY?: number;
  mouseEnterDelay?: number;
  mouseLeaveDelay?: number;
  onOpen?: Function;
  onClose?: Function;
  open?: boolean;
  defaultOpen?: boolean;
  trigger?: React.ReactElement<Node>;
  on?: 'hover' | 'click' | 'focus' | Array<'hover' | 'click' | 'focus'>;
  position?: 'top left' | 'top center' | 'top right' | 'bottom left' | 'bottom center' | 'bottom right' | 'right top' | 'right center' | 'right bottom' | 'left top' | 'left center' | 'left bottom';
}

declare const Popover: React.ComponentType<PopoverProps>;

export default Popover;