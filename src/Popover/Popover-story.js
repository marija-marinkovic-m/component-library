import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { selectV2 as select, text, number, boolean } from '@storybook/addon-knobs/react';

import Popover from './Popover';
import TableComponent from '../util/storybook-TableComponent';
import styled from 'styled-components';

const Menu = styled.div`
  width: 200px; display: flex;
  background: white;
  flex-direction: column;

  .menu-item {
    height: 28px; padding: 5px;
    border-bottom: 1px solid rgb(187,187,187);
    cursor: pointer;
    &:hover {
      color: #2980b9;
    }
  }
`;

storiesOf('Components | Popover', module)
  .add('Modal', withInfo({TableComponent})(() => {
    const story = (
      <Popover
        trigger={<button>Open Modal</button>}
        modal
        closeOnDocumentClick>
        <p className="content-p">Modal content goes here...</p>
      </Popover>
    );

    return story;
  }))
  .add('Tooltip', () => {
    const triggerOnOptions = {
      Hover: 'hover',
      Click: 'click',
      Focus: 'focus'
    };
    const triggerOn = select('Trigger event', triggerOnOptions, 'hover');
    console.log('triggerOn', triggerOn);
    return (
      <Popover
        trigger={open => <button>Trigger btn</button>}
        on={triggerOn}
        position="right center"
        closeOnDocumentClick>
        <div>{text('Tooltip Content', 'Tooltip content here')}</div>
      </Popover>
    )
  })
  .add('Uncontrolled popover', () => {
    return <Popover defaultOpen={true} position="top left"><span>Uncontrolled component content</span></Popover>
  })
  .add('Menu', () => (
    <Menu>
      <div className="menu-item"> Menu item 1</div>
      <div className="menu-item"> Menu item 2</div>
      <div className="menu-item"> Menu item 3</div>
      <Popover
        trigger={<div className="menu-item"> Has SubMenu </div>}
        position="right top"
        on="hover"
        closeOnDocumentClick
        mouseLeaveDelay={300}
        mouseEnterDelay={0}
        arrow={false}
      >
        <div className="menu">
          <div className="menu-item"> item 1</div>
          <div className="menu-item"> item 2</div>
          <div className="menu-item"> item 3</div>
        </div>
      </Popover>
      <div className="menu-item"> Menu item 4</div>
    </Menu>
  ));