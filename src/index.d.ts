import * as React from 'react';

/**
 * All standard components exposed by `m-ui-components`
 * can have `className` and inline `style`
 */
export type StandardProps<T> = {
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode | Array<React.ReactNode>;
  theme?: Object;
};

export namespace PropTypes {
  type Alignment = 'inherit' | 'left' | 'center' | 'right' | 'justify';
  type Color = 'inherit' | 'primary' | 'secondary' | 'default';
  type Margin = 'none' | 'dense' | 'normal';
}

export { default as AppBar } from './AppBar';
export { default as Avatar } from './Avatar';
export { default as Icon } from './Icon';
export { default as Button } from './Button';
export { default as Popover } from './Popover';

// form subcomponents
export { Addon, Hint, Label, Field, MultipleInputWrap } from './Form';
export { Input, UncontrolledInput } from './Input';