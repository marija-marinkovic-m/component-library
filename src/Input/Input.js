import React from 'react';
import PropTypes from 'prop-types';
import UncontrolledInput from './UncontrolledInput';

export default class Input extends React.Component {
  static displayName = 'Input';
  static propTypes = {
    type: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    icon: PropTypes.object,
    iconPosition: PropTypes.oneOf(['left', 'right']),
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    invalid: PropTypes.bool,
    autoFocus: PropTypes.bool
  }
  static defaultProps = {
    type: 'text'
  }
  state = {
    value: this.props.value || ''
  }

  handleChange = (event) => {
    this.setState({value: event.target.value});
    if (this.props.onChange) this.props.onChange(event);
  }

  _inputRef;
  focus = () => {
    this._inputRef.focus();
  }

  render() {
    return (
      <UncontrolledInput
        onChange={this.handleChange}
        value={this.state.value}
        {...this.props}
        ref={el => {this._inputRef = el}} />
    );
  }
}