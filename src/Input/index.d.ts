export { default as Input } from './Input';
export { default as UncontrolledInput } from './UncontrolledInput';

export * from './Input';
export * from './UncontrolledInput';