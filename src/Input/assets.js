export const baseInputStyles = {
  padding({icon, iconPosition}) {
    if (icon) {
      if (iconPosition === 'right') {
        return `padding-right: 2.2em;`;
      }
      return `padding-left: 2.2em;`;
    }
    return null;
  },
  iconPosition({iconPosition}) {
    if (iconPosition === 'left') {
      return `left: 0.5rem; color: #c3d1eb;`;
    }
    return `right: 0.5rem; cursor: pointer; &:hover {color: #c4d2eb;}`;
  }
}