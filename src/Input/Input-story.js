import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import { boolean, text } from '@storybook/addon-knobs';

import Input from './Input';

import fontawesome from '@fortawesome/fontawesome';
import light from '@fortawesome/fontawesome-pro-light';
import { UncontrolledInput } from '..';

fontawesome.library.add(light);

const iconProps = {
  glyph: ['fal', 'search']
}

storiesOf('Components | Input', module)
  .add('Basic', withInfo({
    text: `Input description`
  })(() => {
    return <Input
      value={text('Value', 'Test value')}
      icon={iconProps}
      disabled={boolean('Disabled', false)}
      iconPosition="right" />
  }))
  .add('Uncontrolled Input', withInfo({
    text: 'Uncontrolled input description...'
  })(() => {
    return <UncontrolledInput
      defaultValue="test" />
  }));