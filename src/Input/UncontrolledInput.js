import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import baseStyle from '../Form/FieldBase';
import { baseInputStyles } from './assets';
import Icon from '../Icon';


const InputElement = styled.input`
  ${baseStyle};
  vertical-align: middle;
  height: 1.93267rem;
  ${baseInputStyles.padding};
`;

const InputIcon = styled(Icon)`
  position: absolute; z-index: 7; top: calc(50% - 0.5em);
  width: 2.2em;
  ${baseInputStyles.iconPosition};
  ${({disabled}) => disabled && `
    pointer-events: none;
    opacity: 0.3;
  `}
`;

class UncontrolledInputComponent extends React.Component {
  _inputRef;

  onFocus = () => {
    this._inputRef.focus();
  }

  render() {
    const { icon, iconPosition, disabled } = this.props;

    return (<div {...this.props}>
      <InputElement
        innerRef={el => {this._inputRef = el;}}
        {...this.props} />
      { icon && <InputIcon iconPosition={iconPosition} {...icon} disabled={disabled} /> }
    </div>);
  }
}

const UncontrolledInput = styled(UncontrolledInputComponent)`
  position: relative;
`;

UncontrolledInput.displayName = 'UncontrolledInput';
UncontrolledInput.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  icon: PropTypes.object,
  iconPosition: PropTypes.oneOf(['left', 'right']),
  defaultValue: PropTypes.string,
  disabled: PropTypes.bool,
  invalid: PropTypes.bool,
  autoFocus: PropTypes.bool
};
UncontrolledInput.defaultProps = {
  type: 'text',
  iconPosition: 'left'
};

export default UncontrolledInput;