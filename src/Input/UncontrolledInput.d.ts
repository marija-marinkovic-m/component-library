import * as React from 'react';
import { StandardProps } from '..';

import { IconProps } from '../Icon/Icon';

export interface UncontrolledInputProps extends StandardProps<React.HTMLAttributes<HTMLDivElement | HTMLInputElement>> {
  type?: string;
  placeholder?: string;
  value: string;
  icon?: IconProps;
  iconPosition?: 'left' | 'right';
  defaultValue?: string;
  disabled?: boolean;
  invalid?: boolean;
  autoFocus?: boolean;
}

declare const UncontrolledInput: React.ComponentType<UncontrolledInputProps>;

export default UncontrolledInput;