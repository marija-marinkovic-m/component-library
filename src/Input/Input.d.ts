import * as React from 'react';
import { StandardProps } from '..';

import { IconProps } from '../Icon/Icon';

export interface InputProps extends StandardProps<React.HTMLAttributes<HTMLDivElement | HTMLInputElement>> {
  type?: string;
  placeholder?: string;
  value: string;
  icon?: IconProps;
  iconPosition?: 'left' | 'right';
  onChange?: Function;
  disabled?: boolean;
  invalid?: boolean;
  autoFocus?: boolean;
}

declare const Input: React.ComponentType<InputProps>;

export default Input;