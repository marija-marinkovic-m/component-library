import React from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';

class Ref extends React.PureComponent {
  componentDidMount() {
    const { innerRef } = this.props;
    if (innerRef) innerRef(findDOMNode(this));
  }
  render() {
    const { children } = this.props;
    return React.Children.only(children);
  }
}

Ref.displayName = 'RefComponent';

Ref.propTypes = {
  innerRef: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.element])
};

export default Ref;
