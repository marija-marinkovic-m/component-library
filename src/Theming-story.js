import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

storiesOf('Theming', module)
  .add('Design Tokens', () => (
    <div>
      <h1>...</h1>
      <p>Design tokens are the visual design atoms of the design system — specifically, they are named entities that store visual design attributes. We use them in place of hard-coded values (such as hex values for color or pixel values for spacing) in order to maintain a scalable and consistent visual system for UI development.</p>

      <code>
      Colors
      Dimensions
      Fonts
      Font size
      Font weight
      Line height
      Media Queries
      Spacing
      Timing
      Z-index
      </code>
    </div>
  ))
  .add('Colors', () => <div><p>Use color tokens to change the color of backgrounds, borders, links and text. Make sure to use the correct token type for the corresponding property.</p> </div>)
  .add('Fonts', () => <div><p>Use font size tokens to set the text size.</p></div>)
  .add('Typography', () => <div><p>typography...</p></div>)
  .add('Icons', () => <div>Icons here...</div>);
