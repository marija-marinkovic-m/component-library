import * as React from 'react';
import { StandardProps } from '..';

declare const MultipleInputWrap: React.ComponentType<StandardProps<React.HTMLAttributes<HTMLDivElement>>>;
export default MultipleInputWrap;