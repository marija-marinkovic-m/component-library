import * as React from 'react';
import { StandardProps } from '..';

declare const Addon: React.ComponentType<StandardProps<React.HTMLAttributes<HTMLDivElement>>>;
export default Addon;