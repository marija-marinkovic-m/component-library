import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Addon from './Addon';
import { multipleStyles } from './assets';

const FieldCell = styled.div`
  display: table-cell;
  vertical-align: middle;

  > * {
    width: calc(100% + 2px);
  }

  &:not(:first-child) > * {
    margin-left: 1px; margin-right: 0;
  }

  &:last-child {
    * {
      border-top-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
    }
  }

  &:first-child {
    * {
      border-right: 0;
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
      border-top-left-radius: 4px;
      border-bottom-left-radius: 4px;
    }
  }

  &:not(:first-child):not(:last-child) {
    * {
      border-radius: 0;
    }
  }

  ${multipleStyles.withAddonStyles}
`;

const MultipleInputWrapComponent = ({ children, ...otherProps }) => (
  <div {...otherProps}>
    {children.map((child, index) => (
      <FieldCell key={index} addon={child.type === Addon}>
        {child}
      </FieldCell>
    ))}
  </div>
);

const MultipleInputWrap = styled(MultipleInputWrapComponent)`
  position: relative; display: table;
  width: 100%;
  border-collapse: separate;
`;

MultipleInputWrap.displayName = 'MultipleInputWrap';
MultipleInputWrap.propTypes = {
  children: PropTypes.oneOf([PropTypes.node, PropTypes.arrayOf(PropTypes.node)])
};

export default MultipleInputWrap;