import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { hintStyles } from './assets';

export const kindEnums = ['primary', 'danger', 'warning', 'success'];

const HintComponent = ({text, kind, ...props}) => (<div kind={kind} {...props}>{text}</div>);

const Hint = styled(HintComponent)`
  display: block !important;
  padding-top: 0.4rem;
  font-size: 0.8rem;
  line-height: 1.42857;
  padding-left: 1px;
  white-space: normal;
  color: ${hintStyles.color}
`;

Hint.displayName = 'Hint';
Hint.propTypes = {
  text: PropTypes.string.isRequired,
  kind: PropTypes.oneOf(kindEnums)
};
Hint.defaultProps = {
  text: '',
  kind: 'primary'
};

export default Hint;