import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Label from './Label';
import { fieldStyles } from './assets';

const FieldComponent = ({label, align, inline, required, children, ...otherProps}) => (<div inline={inline} {...otherProps}>
  <Label text={label} required={required} align={align} />
  {children}
</div>);

const Field = styled(FieldComponent)`
  position: relative;
  width: 100%;
  display: ${fieldStyles.display};
  margin-bottom: ${fieldStyles.marginBottom};

  ${fieldStyles.directSpan}
`;

Field.displayName = 'Field';
Field.propTypes = {
  label: PropTypes.string.isRequired,
  align: PropTypes.oneOf(['left' | 'right']),
  inline: PropTypes.bool,
  required: PropTypes.bool,
  children: PropTypes.node
};
Field.defaultProps = {
  label: 'Form Label',
  align: 'left',
  inline: false,
  required: false
}

export default Field;