import { css } from 'styled-components';
import { fieldBaseStyles } from './assets';

const FieldBase = () => css`
  position: relative; z-index: 5;
  display: block;
  width: 100%;
  padding: 0.46633rem 0.8rem;
  color: ${fieldBaseStyles.color}; font-size: 1rem; line-height: 1;
  background-color: #ffffff;
  border: 1px solid ${fieldBaseStyles.borderColor}; border-radius: 4px;
  box-sizing: border-box;
  outline: 0
  transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;

  &::placeholder {
    color: #95acd4;
  }

  &::focus {
    z-index: 7;
    border: 1px solid ${fieldBaseStyles.focusBorderColor};
    box-shadow: 0 0 0 2px rgba(14,134,254,0.2);
    outline: 0;
  }

  &:disabled {
    pointer-events: none;
    border-color: ${fieldBaseStyles.disabledBorderColor};
    background-color: ${fieldBaseStyles.disabledBackgroundColor};
    color: ${fieldBaseStyles.disabledBorderColor}

    &::placeholder {color: ${fieldBaseStyles.disabledBorderColor}}
  }
`;

export default FieldBase;