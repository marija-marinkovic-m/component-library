import * as React from 'react';
import { StandardProps } from '..';

export interface HintProps extends StandardProps<React.HTMLAttributes<HTMLDivElement>> {
  text: string;
  kind?: 'primary' | 'danger' | 'warning' | 'success';
}

declare const Hint: React.ComponentType<HintProps>;

export default Hint;