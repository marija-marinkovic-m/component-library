import * as React from 'react';
import { StandardProps } from '..';

export interface FieldProps extends StandardProps<React.HTMLAttributes<HTMLDivElement>> {
  label: string;
  align: 'left' | 'right';
  inline?: boolean;
  required?: boolean;
}

declare const Field: React.ComponentType<FieldProps>;

export default Field;