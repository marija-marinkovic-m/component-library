import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { action } from '@storybook/addon-actions';

import Typography from '../Typography';
import { Form, Field, MultipleInputWrap, Addon } from '.';
import Button from '../Button';
import { Input } from '../Input';
import { TextArea } from '../TextArea';


storiesOf('Blocks | Form', module)
  .add('Default', withInfo(`
    Form component describe...
  `)(() => (
    <Form style={{width: '480px'}} onSubmit={e => action('Form submit...')}>

      <Typography variant="headline" align="center" gutterBottom={true}>My Profile</Typography>
      
      <Field required label="Username">
        <Input placeholder="Enter your username" />
      </Field>

      <Field required label="Full name">
        <MultipleInputWrap>
          <Input placeholder="First Name" />
          <Input placeholder="Last Name" />
        </MultipleInputWrap>
      </Field>

      <Field label="Birthday">
        <MultipleInputWrap>
          <Input placeholder="Month" />
          <Input placeholder="Day" />
          <Input placeholder="Year" />
        </MultipleInputWrap>
      </Field>

      <Field label="Website">
        <MultipleInputWrap>
          <Addon>https://</Addon>
          <Input placeholder="Label addon on the left" />
        </MultipleInputWrap>
      </Field>

       <Field label="Additional" inline>
        <TextArea placeholder="Additional information..." rows={3} />
      </Field>
      
    </Form>
  )));