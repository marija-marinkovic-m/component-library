import React from 'react';
import styled from 'styled-components';

const AddonComponent = ({children, ...props}) => <div {...props}>{children}</div>;

const Addon = styled(AddonComponent)`
  position: relative; z-index: 5;
  padding: 0.46633rem 0.8rem;
  height: 1.93267rem;
  font-size: 1rem; font-weight: 400; line-height: 1; color: #7189b6; text-align: center;
  background-color: #eef2fc;
  border: 1px solid #c4d2eb; border-radius: 4px;
  box-sizing: border-box;
`;

Addon.displayName = 'Addon';

export default Addon;