export const theTheme = (theme) => {
  const defaultTheme = {
    palette: {
      primary: 'deepskyblue',
      secondary: 'deeppink',
      danger: 'red',
      alert: 'orange',
      success: 'darkolivegreen',
      white: 'white'
    },
    textPrimary: 'rgba(31,41,71,0.87)',
    textSecondary: 'rgba(31,41,71,0.37)',
    textMuted: 'lightgrey'
  };
  return Object.assign({}, defaultTheme, theme);
}

export const hintStyles = {
  color({kind, theme}) {
    const { palette } = theTheme(theme);
    return palette[kind] || palette.primary;
  }
}

export const labelStyles = {
  textAlign({align}) {
    return align;
  },
  required({required}) {
    return required && `
      :before {
        content: '';
        position: absolute; left: -0.666rem; top: 0.666rem;
        display: inline-block; vertical-align: middle;
        width: 0.266rem; height: 0.266rem;
        margin: -0.133rem 0.533rem 0 0;
        background-color: #EB5000;
        border-radius: 50%;
      }
    `
  },
  inline({inline}) {
    return inline && `
      width: 1%; min-width: 8rem;
      vertical-align: top;
      padding-right: 1.2rem; padding-top: calc(0.46633rem + 2px);
      line-height: 1;
      overflow: hidden; text-overflow: ellipsis; white-space: nowrap;
    `;
  }
}

export const fieldBaseStyles = {
  color({theme}) {
    return theTheme(theme).textPrimary;
  },
  borderColor({kind, theme, invalid}) {
    return kind === 'warning' || invalid ? '#EB5000' : theTheme(theme).textMuted;
  },
  disabledBorderColor({kind, theme, invalid}) {
    return kind === 'warning' || invalid ? '#EB5000' : '#c4d2eb';
  },
  disabledBackgroundColor({kind, theme, invalid}) {
    return kind === 'warning' || invalid ? '#FFD0B8' : '#f5f5f5';
  },
  focusBorderColor: '#0e86fe'
};

export const fieldStyles = {
  display({inline}) {
    return inline ? 'table' : 'block';
  },
  marginBottom({inline}) {
    return inline ? '1.866rem' : '1.333rem';
  },
  directSpan({inline}) {
    return inline && `
      > span {
        display: table-cell; vertical-align: top;
        width: 1%; min-width: 8rem;
        padding-top: calc(0.46633rem + 2px); padding-right: 1.2rem;
        line-height: 1;
        overflow: hidden; text-overflow: ellipsis; white-space: nowrap;
      }
    `
  }
}

export const multipleStyles = {
  withAddonStyles({addon}) {
    return addon && `
      width: 1%; white-space: nowrap;
    `
  }
}