import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const FormComponent = ({children, ...other}) => (
  <form {...other}>
    {' '}{children}{' '}
  </form>
);

/** Form component description */
const Form = styled(FormComponent)`
  margin: 0 0 20px;
`;

Form.propTypes = {
  /** children */
  children: PropTypes.node
};

Form.displayName = 'Form';

export default Form;