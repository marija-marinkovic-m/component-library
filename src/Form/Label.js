import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { labelStyles } from './assets';

const LabelComponent = ({text, ...otherProps}) => (<span {...otherProps}>
  {text}
</span>);

const Label = styled(LabelComponent)`
  position: relative; display: inline-block;
  font-size: 0.866rem; line-height: 1.8;
  text-align: ${labelStyles.textAlign}; color: #7189b6;
    ${labelStyles.required}
    ${labelStyles.inline}
`;

Label.displayName = 'Label';
Label.propTypes = {
  text: PropTypes.string.isRequired,
  required: PropTypes.bool,
  inline: PropTypes.bool,
  align: PropTypes.oneOf(['left', 'right'])
};
Label.defaultProps = {
  text: '',
  align: 'left',
  required: false,
  inline: false
};

export default Label;