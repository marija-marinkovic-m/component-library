import * as React from 'react';
import { StandardProps } from '..';

export interface LabelProps extends StandardProps<React.HTMLAttributes<HTMLSpanElement>> {
  text: string;
  required?: boolean;
  inline?: boolean;
  align?: 'left' | 'right'
}

declare const Label: React.ComponentType<LabelProps>;

export default Label;