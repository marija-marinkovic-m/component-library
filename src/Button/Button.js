import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

import { styles } from './assets';

export const kindEnums = ['primary', 'secondary', 'danger', 'warning', 'success', 'ghost', 'base'];

/**
 * Button component description
 * @param {object} props  
 */
function ButtonComponent(props) {
  const {
    children,
    component,
    type,
    disabled,
    buttonRef,
    ...other
  } = props;

  const buttonProps = {}

  let ComponentProp = component || (other.href ? 'a' : 'button');

  if ('button' === ComponentProp) {
    buttonProps.type = type || 'button';
    buttonProps.disabled = disabled;
  } else {
    buttonProps.role = 'button';
  }

  return (
    <ComponentProp
      {...buttonProps}
      {...other}
      ref={buttonRef}>
      {children}
    </ComponentProp>
  );
}

const Button = styled(ButtonComponent) `
  position: relative; z-index: 2;
  display: inline-flex; align-items: center; justify-content: center;
  vertical-align: middle;
  margin: 0;
  padding: ${styles.padding};
  font-size: ${styles.fontSize}; text-align: center; line-height: 1;
  color: ${styles.color}; text-transform: capitalize;
  background-color: ${styles.backgroundColor};
  border-width: ${styles.borderWidth}; border-color: ${styles.borderColor.bind(styles)}; border-style: solid; border-radius: ${styles.borderRadius};
  text-decoration: none;
  opacity: ${styles.opacity};
  box-sizing: border-box;
  cursor: ${styles.cursor}; user-select: none;
  outline: none;
  &:disabled {
    pointer-events: none;
  }
  &::before {
    content: "";
    position: absolute; z-index: -1; top: 0; left: 0; right: 0; bottom: 0;
    background: ${styles.hoverBackgroundColor};
    border-radius: ${styles.borderRadius};
    transform: scaleY(0); transform-origin: 50% 100%; transition-property: transform;transition-duration: .03s; transition-timing-function: ease-out;
  }
  &:hover, &:focus, &:active {color: ${styles.hoverColor.bind(styles)};}
  &:hover:before, &:focus:before, &:active:before {
    ${({disabled}) => !disabled && 'transform: scaleY(1)'}
  } 
`;

Button.displayName = 'Button';

Button.propTypes = {
  children: PropTypes.node.isRequired,
  disabled: PropTypes.bool,
  /**
   * The component used for the root node
   * */
  component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  /**
   * URL to link to, if defined, and `a` element will be the root node
   * */
  href: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  type: PropTypes.oneOf(['button', 'reset', 'submit']),
  /**
   * use this prop to pass a ref callback to the native button component
   * */
  buttonRef: PropTypes.func,
  kind: PropTypes.oneOf(kindEnums),
  theme: PropTypes.object
}

Button.defaultProps = {
  type: 'button',
  kind: 'base',
  size: 'medium',
  disabled: false,
  theme: {}
}

export default Button;