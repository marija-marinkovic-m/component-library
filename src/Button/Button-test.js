import { shallow, mount, render } from 'enzyme';
import React from 'react';
import PropTypes from 'prop-types';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import Button from './Button';

describe('Button', () => {
  describe('render common props as expected', () => {
    const button = shallow(
      <Button className="extra-class" tabIndex={2}>
        <div className="child">child</div>
        <div className="child">child</div>
      </Button>
    );

    it('should render children as expected', () => {
      expect(button.find('.child').length).toBe(2);
    });
    it('should set tabIndex if one is passed via props', () => {
      expect(button.props().tabIndex).toEqual(2);
    });
    it('should add extra classes via className prop', () => {
      expect(button.hasClass('extra-class')).toBe(true);
    });
  });
  describe('apply correct `component` as base element', () => {
    const defaultButton = render(
      <Button>Default Button</Button>
    );
    const linkButton = render(
      <Button className="extra-class" tabIndex={2} href="/front" target="_blank">Link button</Button>
    );
    const componentButton = render(
      <Button component="div" aria-checked={false} role="checkbox">Component string Button</Button>
    );

    const Paragraph = (props) => <p {...props} />;    
    const paragraphButton = render(
      <Button component={Paragraph}>Component function button</Button>
    );

    it('should render as <button> by default', () => {
      expect(defaultButton.is('button')).toBe(true);
    });
    it('should render as <a> with `href` property provided', () => {
      expect(linkButton.is('a')).toBe(true);
    });
    it('should render as <div> with prop component="div"', () => {
      expect(componentButton.is('div')).toBe(true);
    });
    it('should render as <p> with prop component={() => <p />}', () => {
      expect(paragraphButton.is('p')).toBe(true);
    });
    it('should always render with [role="button"] by default', () => {
      expect(linkButton.attr('role')).toBe('button');
      expect(paragraphButton.attr('role')).toBe('button');
      // role property provided
      expect(componentButton.attr('role')).toBe('checkbox');
    });
  });
  describe('render <button> props as expected', () => {
    const button = shallow(
      <Button tabIndex={2}>
        Button Component
      </Button>
    );

    it('should set disabled to false by default', () => {
      expect(button.props().disabled).toBe(false);
    });
    it('should set disabled if one is passed via props', () => {
      button.setProps({ disabled: true });
      expect(button.props().disabled).toBe(true);
    });
    it('should set type to `button` by default', () => {
      expect(button.props().type).toEqual('button');
    });
    it('should only set type to `button`, `reset` or `submit` if one is passed via props', () => {
      button.setProps({ type: 'submit' });
      expect(button.props().type).toEqual('submit');
    });
  });

  describe('property: buttonRef', () => {
    it('should be able to get a ref of the root element', () => {
      function ButtonRefComp(props) {
        return <Button buttonRef={props.rootRef}>{props.children}</Button>;
      }
      ButtonRefComp.propTypes = {
        rootRef: PropTypes.func.isRequired
      };

      const customProps = {
        _node: null,
        ref(node) {
          this._node = node;
        }
      };
      const spyRef = spyOn(customProps, 'ref');

      mount(<ButtonRefComp rootRef={spyRef}>Label</ButtonRefComp>);

      expect(spyRef).toHaveBeenCalled();
      expect(spyRef.calls.mostRecent().args[0].type).toEqual('button');
    });
  })
});

describe('Button Variants', () => {

  describe('should render button `kind` & `size` properly', () => {
    const primarySmallButton = renderer.create(<Button kind="primary" size="small">Primary Button</Button>);
    const dangerLinkButton = renderer.create(<Button href="/home" kind="danger">Danger Link Button</Button>);

    it('primary small button snapshoot', () => {
      const psb = primarySmallButton.toJSON();
      expect(psb).toMatchSnapshot();
    });
    it('danger link button snapshoot', () => {
      const dlb = dangerLinkButton.toJSON();
      expect(dlb).toMatchSnapshot();
    });
  });
});

describe('Button Theming', () => {
  const customTheme = {
    palette: {
      primary: 'red'
    }
  };
  const button = renderer.create(<Button theme={customTheme}>Custom Theme</Button>).toJSON();
  const buttonDisabled = renderer.create(<Button theme={customTheme} disabled={true}>Disabled Button</Button>).toJSON();

  describe('should render custom theme prop', () => {
    it('should have style rule `background-color: red`', () => {
      expect(button).toHaveStyleRule('background-color', 'red');
    });

    it('should have style rule `opacity: .4` for disabled', () => {
      expect(buttonDisabled).toHaveStyleRule('opacity', '.4');
    });

    it('should have `cursor: not-allowed` for disabled, `cursor: pointer` otherwise', () => {
      expect(button).toHaveStyleRule('cursor', 'pointer');
      expect(buttonDisabled).toHaveStyleRule('cursor', 'not-allowed');
    });
  });
});