import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { selectV2 as select } from '@storybook/addon-knobs';

import Button from './Button';
import Icon from '../Icon';
import faGlobe from '@fortawesome/fontawesome-pro-light/faGlobe';

import { kindEnums } from './Button';
const variations = [
  {size: 'small'},
  {size: 'medium'},
  {size: 'large'},
  {disabled: true}
];

storiesOf('Components | Button', module)
  .add('Button Variations', withInfo(`
    Buttons are used to initialize an action, either in the background or
    foreground of an experience.

    Primary buttons should be used for the principle call to action
    on the page.
    
    Modify the behavior of the button by changing its event properties.

    Small buttons may be used when there is not enough space for a
    regular sized button. This issue is most found in tables.

    Disabled Buttons may be used when the user cannot proceed until input is collected.
  `)(() => {
    const kindOptions = kindEnums.reduce((acc,n) => {acc[n] = n; return acc}, {});
    const kind = select('Kind', kindOptions, 'base', 'BTNKIND');
    return (
      <div>
        { variations.map(props => (
          <Button {...props} kind={kind}>
            { kind } { props.size === 'medium' ? 'Button' : (props.disabled ? 'Disabled' : props.size) }
          </Button>
        )).reduce((p,c) => [p, <i>&nbsp;</i>, c]) }
      </div>
    );
  }))
  .add('Button Sets', withInfo(`
    When an action required by the user has more than one option, always use a a negative action button (secondary) paired with a positive action button (primary) in that order.
    Negative action buttons will be on the left.
    Positive action buttons should be on the right.
    
    When these two types buttons are paired in the correct order, they will automatically space themselves apart.`)(() => (
    <div>
      <Button kind="secondary" className="some-class">
        Secondary button
      </Button>&nbsp;
      <Button kind="primary" className="some-class">
        Primary button
      </Button>
    </div>
  )))
  .add('Icon Buttons', withInfo(`Icon buttons`)(() => (
    <div>
      <Button kind="ghost"><Icon glyph={ faGlobe } /></Button>&nbsp;
      <Button><Icon glyph={faGlobe} />&nbsp;Button content</Button>
    </div>
  )));