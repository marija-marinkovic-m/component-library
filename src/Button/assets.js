export const theTheme = (theme) => {
  const defaultTheme = {
    palette: {
      primary: 'deepskyblue',
      secondary: 'deeppink',
      danger: 'red',
      alert: 'orange',
      success: 'darkolivegreen',
      white: 'white',
      gray: {
        light: '#fafafa',
        medium: '#a2a2a2',
        dark: '#212121',
      }
    },

    textPrimary: 'rgba(31,41,71,0.87)',
    textSecondary: 'rgba(31,41,71,0.37)',
    textMuted: 'lightgrey' 
  };
  return Object.assign({}, defaultTheme, theme);
}

export const styles = {
  padding({ size }) {
    switch (size) {
      case 'small':
        return '.6em .95em';
      case 'large':
        return '.8em 1.45em';
      default: // 'medium'
        return '.65em 1.3em';
    }
  },
  fontSize({ size }) {
    switch (size) {
      case 'small':
        return '0.6em';
      case 'large':
        return '1.35em';
      default: // 'medium'
        return '1em';
    }
  },
  color({ kind, theme }) {
    const { palette } = theTheme(theme);
    switch (kind) {
      case 'ghost':
      case 'secondary':
        return palette.primary;
      case 'warning':
      case 'base':
        return palette.textPrimary;
      default:
        return palette.white;
    }
  },
  hoverColor({ kind, theme, disabled }) {
    if (disabled) return this.color.call(styles, {kind, theme});
    const { palette } = theTheme(theme);
    switch(kind) {
      case 'secondary':
      case 'ghost':
        return palette.white;
      case 'base':
        return palette.textSecondary;
      default:
        return this.backgroundColor.call(styles, { kind, theme });
    }
  },
  backgroundColor({ kind, theme }) {
    const { palette } = theTheme(theme);
    switch (kind) {
      case 'danger':
        return palette.danger;
      case 'ghost':
      case 'secondary':
      case 'base':
        return 'transparent';
      case 'warning':
        return palette.alert;
      case 'success':
        return palette.success;
      default:
        return palette.primary;
    }
  },
  hoverBackgroundColor({ kind, theme }) {
    const { palette } = theTheme(theme);
    switch(kind) {
      case 'secondary':
      case 'ghost':
        return palette.primary;
      case 'base':
        return 'transparent';
      default:
        return palette.white;
    }
  },
  borderColor({ kind, theme }) {
    const { palette } = theTheme(theme);
    switch (kind) {
      case 'secondary':
        return palette.primary;
      case 'base':
        return 'transparent';
      default:
        return this.backgroundColor.call(styles, { kind, theme });
    }
  },
  borderWidth({ size }) {
    switch (size) {
      case 'ghost':
        return '0';
      default:
        return '1px';
    }
  },
  borderRadius({ size }) {
    switch (size) {
      default:
        return '3px';
    }
  },
  opacity({ disabled }) {
    return disabled ? '.4' : '1';
  },
  cursor({ disabled }) {
    return disabled ? 'not-allowed' : 'pointer';
  }
}