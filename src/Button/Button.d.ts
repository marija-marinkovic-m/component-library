import * as React from 'react';
import { StandardProps } from '..';

export interface ButtonProps extends StandardProps<React.HTMLAttributes<HTMLButtonElement | HTMLAnchorElement>> {
  disabled?: boolean;
  component?: string | Function;
  href?: string;
  size?: 'small' | 'medium' | 'large';
  type?: 'button' | 'reset' | 'submit';
  buttonRef?: Function;
  kind?: 'primary' | 'secondary' | 'danger' | 'warning' | 'success' | 'ghost' | 'base';
}

declare const Button: React.ComponentType<ButtonProps>;

export default Button;