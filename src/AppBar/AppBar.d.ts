import * as React from 'react';
import { StandardProps } from '..';

export interface AppBarProps extends StandardProps<React.HTMLAttributes<HTMLDivElement>> {
  color?: 'inherit' | 'primary' | 'secondary' | 'default';
  position?: 'fixed' | 'absolute' | 'sticky' | 'static';
}
declare const AppBar: React.ComponentType<AppBarProps>;

export default AppBar;