import React from 'react';
import { storiesOf } from '@storybook/react';
import AppBar from './AppBar';

storiesOf('Blocks | AppBar', module)
  .add('AppBar', () => (
    <AppBar color="primary" />
  ));
