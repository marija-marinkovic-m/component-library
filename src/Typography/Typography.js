import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const H1 = styled.h1`
  font-weight: normal; margin-bottom: 0;
`;
const Paragraph = styled.p`
  font-size: 14px;
`;

const TypographyComponent = ({
  align,
  className: classNameProp,
  component: componentProp,
  color,
  gutterBottom,
  headlineMapping,
  noWrap,
  paragraph,
  variant,
  ...other
}) => {

  const Component = styled(componentProp || (paragraph ? Paragraph : headlineMapping[variant]) || 'span')`
    text-align: ${align};
    ${color && `color: ${color};`}
    ${noWrap && `white-space: nowrap; overflow: hidden; text-overflow: ellipsis;`}
    ${gutterBottom && `margin-bottom: 0.35em;`}
  `;

  return <Component {...other} />;
};

const Typography = styled(TypographyComponent)`
  display: block;
`;

Typography.propTypes = {
  /**
   * Set the text-align on the component
   */
  align: PropTypes.oneOf(['inherit', 'left', 'right', 'center', 'justify']),
  /**
   * If `true`, the text will have a bottom margin
   */
  gutterBottom: PropTypes.bool,
  /**
   * The content of the component
   */
  children: PropTypes.node,
  /**
   * @ignore
   */
  className: PropTypes.string,
  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   * By default, it maps the variant to a good default headline component.
   */
  component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
   /**
   * We are empirically mapping the variant property to a range of different DOM element types.
   * For instance, h1 to h6. If you wish to change that mapping, you can provide your own.
   * Alternatively, you can use the `component` property.
   */
  headlineMapping: PropTypes.object,
  /**
   * If `true`, the text will not wrap, but instead will truncate with an ellipsis.
   */
  noWrap: PropTypes.bool,
  /**
   * If `true`, the text will have a bottom margin.
   */
  paragraph: PropTypes.bool,
  /**
   * Applies the theme typography styles.
   */
  variant: PropTypes.oneOf([
    'display4',
    'display3',
    'display2',
    'display1',
    'headline',
    'title',
    'subheading',
    'body2',
    'body1',
    'caption',
    'button',
  ]),
  /**
   * Text color
   */
  color: PropTypes.string
};
Typography.defaultProps = {
  align: 'inherit',
  gutterBottom: false,
  headlineMapping: {
    display4: 'h1',
    display3: 'h1',
    display2: 'h1',
    display1: 'h1',
    headline: H1,
    title: 'h2',
    subheading: 'h3',
    body2: 'aside',
    body1: 'p',
  },
  noWrap: false,
  paragraph: false,
  variant: 'body1'
};


Typography.displayName = 'Typography';

export default Typography;