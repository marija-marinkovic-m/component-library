import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import Typography from '../Typography';

storiesOf('Components | Typography', module)
  .add('default', withInfo(`
    Typography component
  `)(() => (
    <div>
      <Typography variant="headline">Headline</Typography>
      <Typography variant="title">Title</Typography>
      <Typography variant="subheading">Subheading</Typography>
      <Typography>Defaults to `body1` variant</Typography>
    </div>
  )));