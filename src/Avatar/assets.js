export const theTheme = (theme) => {
  const defaultTheme = {
    avatarSize: '46px'
  };
  return Object.assign({}, defaultTheme, theme);
}

export const styles = {
  width({imgProps, theme}) {
    return (imgProps && imgProps.width) || theTheme(theme).avatarSize;
  },
  height({imgProps, theme}) {
    return imgProps && imgProps.width ? (imgProps.height || imgProps.width) : theTheme(theme).avatarSize;
  }
}