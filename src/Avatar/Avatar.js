import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { styles } from './assets';

const AvatarComponent = (props) => {
  const {
    alt,
    children: childrenProp,
    component: Component,
    imgProps,
    sizes,
    src,
    srcSet,
    ...other
  } = props;


  let children = null;

  if (childrenProp) {
    if (
      typeof childrenProp !== 'string' &&
      React.isValidElement(childrenProp)
    ) {
      children = React.cloneElement(childrenProp);
    } else {
      children = childrenProp;
    }
  } else if (src || srcSet) {
    children = (
      <img
        alt={alt}
        src={src}
        srcSet={srcSet}
        sizes={sizes}
        {...imgProps}
      />
    );
  }

  return (
    <Component {...other}>
      {children}
    </Component>
  );
}

const Avatar = styled(AvatarComponent)`
  position: relative;
  display: flex;
  align-items: center; justify-content: center;
  flex-shrink: 0; width: ${styles.width}; height: ${styles.height};
  font-size: 20px; color: red;
  border-radius: 50%; overflow: hidden; user-select: none;

  img {
    width: 100%; height: 100%;
    text-align: center;
    object-fit: cover;
  }
`;

Avatar.propTypes = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: PropTypes.string,
  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: PropTypes.node,
  /**
   * @ignore
   */
  className: PropTypes.string,
  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: PropTypes.object,
  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: PropTypes.string,
  /**
   * The `src` attribute for the `img` element.
   */
  src: PropTypes.string,
  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: PropTypes.string,
};

Avatar.defaultProps = {
  component: 'div',
};

Avatar.displayName = 'Avatar';

export default Avatar;