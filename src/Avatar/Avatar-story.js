import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import Icon from '../Icon';
import faFolderOpen from '@fortawesome/fontawesome-pro-solid/faFolderOpen';

import Avatar from './Avatar';
import Row from '../util/Row';
import styled from 'styled-components';

const MarginAvatar = styled(Avatar)`
  margin: 10px;
`;
const OrangeAvatar = styled(MarginAvatar)`
  background-color: orangered;
  color: whitesmoke;
`;
const GreenAvatar = styled(MarginAvatar)`
  background-color: olivedrab;
  color: white;
`;

storiesOf('Components | Avatar', module)
  .add('Image avatars', withInfo({
    text: `
       Avatars can be used everywhere, from tables to dialog menues.  
       &nbsp;  
       _Image avatars_ can be created by passing standard \`img\` props \`src\` or \`srcSet\` int the component.  
    `,
    propTablesExclude: [Row]
  })(() => {
    return (<Row>
      <Avatar src="http://i.ebayimg.com/images/g/~0YAAOxyldpR781C/s-l300.jpg" />
    </Row>);
  }))
  .add('Icon avatars', withInfo({
    text: `
       Icon avatars are created by passing an icon as \`children\`.  
    `,
    propTables: [Avatar],
    propTablesExclude: [GreenAvatar, Row]
  })(() => {
    return (<Row>
      <GreenAvatar>
        <Icon glyph={faFolderOpen} />
      </GreenAvatar>
    </Row>);
  }))
  .add('Letter avatars', withInfo({
    text: `
      Avatars containing simple characters can be created by passing your string as \`children\`.  
    `,
    propTables: [Avatar],
    propTablesExclude: [MarginAvatar, OrangeAvatar, GreenAvatar, Row]
  })(() => {
    return (<Row>
      <OrangeAvatar>H</OrangeAvatar>
      <GreenAvatar>OP</GreenAvatar>
    </Row>);
  }));