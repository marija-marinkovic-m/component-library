require('dotenv').config();
const path = require('path');
const fse = require('fs-extra');

const toBeDeleted = [
  'node_modules',
  '.storybook',
  'package.json',
  'src',
  '.babelrc',
  'bump-version.sh',
  'jest.config.js',
  'jest.setup.js',
  'tsconfig.json',
  'webpack.babel.js',
  // 'CHANGELOG.md', 'VERSION', 'theme.config.js', 'bundle.min.js'
];

function toRemove() {
  try {
    return fse.readdirSync(path.resolve(__dirname, '..'));
  } catch(e) {
    console.log(e);
  }

  return [];
}

function moveBuild() {
  const buildContent = path.resolve(__dirname, '../build');
  fse.moveSync(buildContent, path.resolve(__dirname, '..'));
}

function run() {
  const structure = toRemove();
  structure
    .filter(el => toBeDeleted.indexOf(el) > -1)
    .map(f => {
      try {
        const filePath = path.resolve(__dirname, '..', f);
        fse.removeSync(filePath);
        console.log('File deleted: ' + filePath);
      } catch (e) {
        console.log(e);
      }
    });
}

if (process.env.UI_LIB != 1) {
  run();
} else {
  console.log('INSIDE m-ui-components UI_LIB');
}