const path = require('path');
const fse = require('fs-extra');

function run() {
  const buildPath = path.resolve(__dirname, '../build');

  fse.remove(buildPath)
    .then(res => {
      console.log(`Successfully deleted: ${buildPath}`);
    })
    .catch(err => console.log(err));
}

if (process.env.UI_LIB != 1) {
  run();
} else {
  console.log('INSIDE m-ui-components UI_LIB');
}
