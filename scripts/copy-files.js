const path = require('path');
const fse = require('fs-extra');
const glob = require('glob');

function copyFile(file) {
  const buildPath = path.resolve(__dirname, '../build/', path.basename(file));
  fse.copySync(file, buildPath);
  console.log('Copied file ' + file + ' to ' + buildPath);
}

function typescriptCopy(from, to) {
  const files = glob.sync('**/*.d.ts', { cwd: from });
  const cmds = files.map(file => fse.copySync(path.resolve(from, file), path.resolve(to, file)));

  return cmds;
}

function createPackageFile() {
  const packageData = fse.readFileSync(path.resolve(__dirname, '../package.json'), 'utf8');
  const parsedData = JSON.parse(packageData);

  const newPackageData = Object.assign({}, parsedData, {
    devDependencies: {},
    scripts: {},
    main: './bundle.min.js',
    module: './index.js',
    private: false,
    types: './index.d.ts'
  });

  const buildPath = path.resolve(__dirname, '../build/package.json');
  fse.writeFileSync(buildPath, JSON.stringify(newPackageData, null, 2), 'utf8');
  console.log('Created package.json in ' + buildPath);

  return newPackageData;
}

function run() {
  ['CHANGELOG.md', 'VERSION', 'theme.config.js', 'bundle.min.js'].map(f => copyFile(f));
  const packageData = createPackageFile();
  const from = path.resolve(__dirname, '../src');
  const tsCopies = typescriptCopy(from, path.resolve(__dirname, '../build'));
}

if (process.env.UI_LIB != 1) {
  run();
} else {
  console.log('INSIDE m-ui-components UI_LIB');
}