import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { checkA11y } from '@storybook/addon-a11y';

import { setOptions } from '@storybook/addon-options';
import { setDefaults } from '@storybook/addon-info';
import { withKnobs } from '@storybook/addon-knobs/react';

import Wrapper from './Wrapper';

// apply knobs
addDecorator(withKnobs);
// check all of the stories violations within components
addDecorator(checkA11y);
// wrap with style
addDecorator(story => <Wrapper story={story} />);

// addon-options
setOptions({
  name: 'm UI Components',
  url: 'https://bitbucket.org/eutelnet/m-ui-components/src/master/',
  goFullScreen: false,
  hierarchySeparator: /\//,
  hierarchyRootSeparator: /\|/,
  addonPanelInRight: false,
  sortStoriesByKind: true,
  sidebarAnimations: false
});
// addon-info
setDefaults({
  inline: true,
  source: true,
  maxPropObjectKeys: 1
});

function loadStories() {
  const req = require.context('../src', true, /\-story\.js$/);
  req.keys().forEach(filename => req(filename));
}

// configure Storybook
configure(loadStories, module);

