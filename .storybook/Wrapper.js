import React from 'react';
import styled, { ThemeProvider } from 'styled-components';

import theme from '../theme.config';

const Div = styled.div`
  padding: .2em 1.2em;
  font-family: ${props => props.theme.fonts.primary || 'sans-serif'};
`;

export default class Wrapper extends React.Component {
  render() {
    const { story } = this.props;

    return (
      <ThemeProvider theme={theme}>
        <Div role="main">{story()}</Div>
      </ThemeProvider>
    );
  }
}