const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: path.resolve(__dirname, './src/index.js'),
  output: {
    filename: 'bundle.min.js',
    libraryTarget: 'umd',
    library: 'm-ui-components',
    umdNamedDefine: true,
  },
  externals: [
    {
      'styled-components': {
        root: 'styled',
        commonjs2: 'styled-components',
        commonjs: ['styled-components'],
        amd: 'styled-components'
      }
    },
    {
      react: {
        root: 'React',
        commonjs2: 'react',
        commonjs: ['react'],
        amd: 'react'
      }
    },
    {
      'react-dom': {
        root: 'ReactDOM',
        commonjs2: 'react-dom',
        commonjs: ['react-dom'],
        amd: 'react-dom'
      }
    },
    {
      'prop-types': {
        root: 'PropTypes',
        commonjs2: 'prop-types',
        commonjs: ['prop-types'],
        amd: 'prop-types'
      }
    },
    {
      '@fortawesome/fontawesome': {
        root: 'fontawesome',
        commonjs2: '@fortawesome/fontawesome',
        commonjs: ['@fortawesome/fontawesome'],
        amd: '@fortawesome/fontawesome'
      }
    }
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: true
        }
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: false,
      debug: false,
    }),
    new webpack.optimize.UglifyJsPlugin({
      beautify: false,
      mangle: {
        screw_ie8: true,
        keep_fnames: true,
      },
      compress: {
        screw_ie8: true,
        warnings: false
      },
      comments: false,
    }),
  ],
};