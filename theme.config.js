const theme = {}

theme.palette = {
  primary: '#438DFB',
  secondary: '#c2185b',
  danger: '#FF3057',
  alert: '#ffa000',
  success: '#388e3c',
  white: 'rgba(255,255,255,0.87)',
  gray: {
    light: '#fafafa',
    medium: '#a2a2a2',
    dark: '#212121',
  },

  textPrimary: 'rgba(31,41,71,0.87)',
  textSecondary: 'rgba(31,41,71,0.37)',
  textMuted: 'lightgrey'
}

theme.fonts = {
  primary: 'Roboto, Helvetica Neue, Helvetica, sans-serif',
  pre: 'Consolas, Liberation Mono, Menlo, Courier, monospace',
  quote: 'Georgia, serif',
}

theme.sizes = {
  maxWidth: '1100px',
}

export default theme