## 1.0.0-beta.14 (May 14, 2018)
  - toBeDeleted var /finalize-build.js script fix

## 1.0.0-beta.13 (May 14, 2018)
  - hierarchy separators; stories reorganization; getting started segment added
  - added user settings; removed umd build from postinstall build script
  - added .env.example
  - Input and FieldBase states updated
  - MultipleField renamed (to MultipleInputWrap); updated Form story basic
  - TextArea (controlled and stateless input) component created
  - added focus method to Input component
  - updated file structure for Input and additional Form components
  - form Input component (+ stateless version)
  - Field and MultipleField components created (as Form subcomponents)
  - Form subcomponents Hint, Addon, Label and FieldBase
  - UI_LIB condition on postinstall
  - Merge branch 'release-1.0.0-beta.11'

## 1.0.0-beta.11 (April 27, 2018)
  - created new postinstall scripts
  - postinstall tweaks: removed umd build from postinstall
  - package devDep=>dep
  - build path for postinstall-build
  - rimraf and postinstall-build dependecies
  - .out rimraffed on build
  - es2015 build script + postinstall command
  - updated bundle (commonjs and commonjs2 paths)
  - updated bundle
  - Merge branch 'release-1.0.0-beta.2'

## 1.0.0-beta.2 (April 26, 2018)
  - defined externals to optimize build; fontawesome token added
  - v1.1.1-beta.0 build
  - Merge branch 'master' of bitbucket.org:eutelnet/m-ui-components
  - Merge branch 'release-1.1.1-beta.0'
  - package version update
  - Merge branch 'hotfix-1.1.1-beta'
  - Merge branch 'release-1.1-beta'

## 1.1.1-beta.0 (April 26, 2018)
  - @types for AppBar, Avatar, Button, Popover
  - FA glyphs moved to src/lib and should not be bundled with package build
  - added AppBar component setup; typescript for typings installed and configured
  - Merge branch 'hotfix-1.1.1-beta' into develop
  - Bumped version number to 1.1.1-beta

## 1.1.1-beta (April 25, 2018)
  - Merge branch 'release-1.1-beta' into develop
  - Bumped version number 1.1.0-beta

## 1.1-beta (April 25, 2018)
  - Avatar story updated (image, letter and icon avatar)
  - Layer component for Icon added; Icon stories
  - Icon Transformation story
  - basic features story - Icon component
  - icon src recomposed; button story reconfigured with knobs; icon button story added; base button kind addded ;
  - avatar component added
  - list & listItem setup
  - icon-stories setup
  - renamed src folder /util
  - fontawesome lib
  - knobs addon integrated (via global decorator); storybook-addon-specifications removed from the project
  - popover tests
  - Popover component; Popover story setup
  - addon-options / global storybook options config; theming story init
  - Merged feature-button-component into develop
  - button test finished; button stories organized; hover states
  - Button tests/ assets
  - added start script
  - Merged release-0.0.1 into master

## 0.1.0 (March 28, 2018)
  - bump version script added
  - moved component src from the main package (m-front)
  - jest/enzyme component tests setup
  - storybook integrated
  - initial commit

